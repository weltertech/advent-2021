const fs = require('fs')

const array = fs.readFileSync('01_input.txt').toString().split('\n')

const left = []
const right = []

for (let i = 0; i < array.length - 1; i++) {
  const [l, r] = array[i].split('   ')
  left.push(parseInt(l.trim(), 10))
  right.push(parseInt(r.trim(), 10))
}

left.sort((a, b) => a > b ? 1 : -1)
right.sort((a, b) => a > b ? 1 : -1)

let td = 0

for (let x = 0; x < left.length; x++) {
  console.log('Left: ', left[x], '   Right: ', right[x], '    Diff: ', left[x] - right[x])
  td += Math.abs(left[x] - right[x])
}

console.log(td)

