const fs = require('fs')

const rows = fs.readFileSync('04_input.txt').toString().split('\n')

const foundLetter = (r, c, letter) => {
  return rows[r] && rows[r].charAt(c) === letter
}

const incr = (v) => { return v + 1 }
const decr = (v) => { return v - 1 }
const same = (v) => { return v }

let count = 0;

for (let r = 0; r < rows.length; r++) {
  for (let c = 0; c < rows[r].length; c++) {
    if (foundLetter(r, c, 'A')) { // found first letter, check all directions
      if ( // not proud of this...
        (foundLetter(decr(r), decr(c), 'M') &&
        foundLetter(decr(r), incr(c), 'S') &&
        foundLetter(incr(r), incr(c), 'S') &&
        foundLetter(incr(r), decr(c), 'M')) ||
        (foundLetter(decr(r), decr(c), 'M') &&
        foundLetter(decr(r), incr(c), 'M') &&
        foundLetter(incr(r), incr(c), 'S') &&
        foundLetter(incr(r), decr(c), 'S')) ||
        (foundLetter(decr(r), decr(c), 'S') &&
        foundLetter(decr(r), incr(c), 'M') &&
        foundLetter(incr(r), incr(c), 'M') &&
        foundLetter(incr(r), decr(c), 'S')) ||
        (foundLetter(decr(r), decr(c), 'S') &&
        foundLetter(decr(r), incr(c), 'S') &&
        foundLetter(incr(r), incr(c), 'M') &&
        foundLetter(incr(r), decr(c), 'M'))
      ) {
        count++
      }
    }
  }
}

console.log(' found: ', count)
