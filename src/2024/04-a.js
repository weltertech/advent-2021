const fs = require('fs')

const rows = fs.readFileSync('04_input.txt').toString().split('\n')

const FIND = 'XMAS'

const foundLetter = (r, c, pos) => {
  return rows[r] && rows[r].charAt(c) === FIND.charAt(pos)
}

const findLetter = (rFn, cFn, r, c, pos) => {
  r = rFn(r)
  c = cFn(c)

  // abort conditions
  if (pos > FIND.length - 1 || r < 0 || r > rows.length - 1 || c < 0 || c > rows[0].length - 1) {
    return false
  }

  // check for next letter
  if (foundLetter(r, c, pos)) {
    if (pos === FIND.length - 1) { // found last letter
      return 1
    }
    return findLetter(rFn, cFn, r, c, pos + 1)
  }
  return false // did not find letter
}

const incr = (v) => { return v + 1 }
const decr = (v) => { return v - 1 }
const same = (v) => { return v }

let count = 0;

for (let r = 0; r < rows.length; r++) {
  for (let c = 0; c < rows[r].length; c++) {
    if (foundLetter(r, c, 0)) { // found first letter, check all directions
      count += findLetter(incr, same, r, c, 1) // look down
      count += findLetter(decr, same, r, c, 1) // look up
      count += findLetter(same, incr, r, c, 1) // look right
      count += findLetter(same, decr, r, c, 1) // look left
      count += findLetter(incr, incr, r, c, 1) // down/right
      count += findLetter(decr, decr, r, c, 1) // up/left
      count += findLetter(incr, decr, r, c, 1)  // down/left
      count += findLetter(decr, incr, r, c, 1) // up/right
    }
  }
}

console.log(FIND, ' found: ', count)
