const fs = require('fs')

const testMapRows = fs.readFileSync('10_test.txt').toString().split('\n')
const prodMapRows = fs.readFileSync('10_input.txt').toString().split('\n')

const testMap = testMapRows.map(r => r.split('').map(c => parseInt(c, 10)))
const prodMap = prodMapRows.map(r => r.split('').map(c => parseInt(c, 10)))

const NEXT = [
  (at) => ({ r: at.r - 1, c: at.c }),
  (at) => ({ r: at.r, c: at.c + 1 }),
  (at) => ({ r: at.r + 1, c: at.c }),
  (at) => ({ r: at.r, c: at.c - 1 })
]

const getMapScore = (m) => {
  console.log(m)
  const COLS = m[0].length
  const ROWS = m.length

  const isPointOnMap = (p) => {
    if (p.c >= COLS || p.c < 0 || p.r >= ROWS || p.r < 0) {
      return false
    }
    return true
  }

  const traverse = (at, summits) => {
    if (m[at.r][at.c] === 9) {
      // console.log('FOUND SUMMIT', at, m[at.r][at.c])
      if (!summits.find(s => s.r === at.r && s.c === at.c)) {
        summits.push(at)
      }
    }

    // console.log('at', at, m[at.r][at.c])
    NEXT.forEach(lookFn => {
      const next = lookFn(at)

      if (isPointOnMap(next) && m[next.r][next.c] - m[at.r][at.c] === 1) {
        // console.log('next', next, m[next.r][next.c])
        traverse(next, summits)
      }
    })
    return summits
  }

  const trailHeads = []
  m.forEach((r, rIdx) => {
    r.forEach((c, cIdx) => {
      if (c === 0) {
        trailHeads.push({ r: rIdx, c: cIdx })
      }
    })
  })

  let score = 0
  trailHeads.forEach(th => {
    console.log('th', th)
    const summits = traverse(th, [])

    score += summits.length
    console.log('summits', summits)
  })
  console.log('score', score)
}

getMapScore(testMap)
getMapScore(prodMap)
