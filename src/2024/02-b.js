const fs = require('fs')

const arr = fs.readFileSync('02_input.txt').toString().split('\n')

const isReportSafe = (r, debug = false) => {
  if (debug) {
    console.log('isSafe', r)
  }
  let allAsc = true
  let allDesc = true
  let okDiff = true 

  for (let i = 0; i < r.length - 1; i++) {
    if (r[i] <= r[i+1]) {
      allDesc = false
    } 

    if (r[i] >= r[i+1]) {
      allAsc = false
    } 

    const diff = Math.abs(r[i] - r[i+1])
    
    if (diff < 1 || diff > 3) {
      okDiff = false
    }
  }

  return (allAsc || allDesc) && okDiff
}

let safeCount = 0
for (let i = 0; i < arr.length - 1; i++) {
  const report = arr[i].split(' ').map(v => parseInt(v, 10))
  if (isReportSafe(report)) {
    safeCount++
  } else {
    // unsafe...try removing levels
    for (let z = 0; z < report.length; z++) {
      const testReport = [...report]
      testReport.splice(z, 1)
      if (isReportSafe(testReport, true)) {
        safeCount++
        break;
      }
    }
  }
}

console.log('Safe: ', safeCount)