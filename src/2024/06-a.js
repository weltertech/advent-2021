const fs = require('fs')

const rows = fs.readFileSync('06_input.txt').toString().split('\n')

const grid = []

const moves = {
  '^': (g) => { return { r: g.r - 1, c: g.c } },
  'v': (g) => { return { r: g.r + 1, c: g.c } }, // eslint-disable-line
  '<': (g) => { return { r: g.r, c: g.c - 1 } },
  '>': (g) => { return { r: g.r, c: g.c + 1 } }
}

const next = {
  '^': '>',
  '>': 'v',
  'v': '<', // eslint-disable-line
  '<': '^'
}

let guard = { r: -1, c: -1, d: '' }

rows.forEach((row, r) => {
  grid.push(row.split('').map((d, c) => {
    if (Object.keys(moves).includes(d)) {
      guard = { r, c, d }
      return d
    }
    return d
  }))
})

console.log(grid)

const isOnGrid = (g) => {
  if (g.r >= grid.length || g.r < 0 || g.c >= grid[0].length || g.c < 0) {
    return false
  }
  return true
}

while (isOnGrid(guard)) {
  const nextPos = moves[guard.d](guard)

  if (grid[nextPos.r] && grid[nextPos.r][nextPos.c] === '#') { // blocked, rotate
    guard.d = next[guard.d]
  } else {
    // mark current spot
    grid[guard.r][guard.c] = guard.d
    // move
    guard = {
      ...guard,
      ...nextPos
    }
  }
}

let usedSpaces = 0
// count the guard's spots
grid.forEach(row => {
  console.log(row.join(''))
  row.forEach(col => {
    if ('<>^v'.indexOf(col) > -1) {
      usedSpaces++
    }
  })
})

console.log('Guard occupied spaces: ', usedSpaces)
