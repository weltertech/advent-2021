const fs = require('fs')

const data = fs.readFileSync('03_input.txt').toString()

const re = /(?:mul\(\d{1,3},\d{1,3}\))|(?:do\(\))|(?:don't\(\))/g

const matches = data.match(re)

let total = 0
let on = true
matches.forEach(m => {
  switch (m) {
    case 'do()': 
      on = true;
      break;
    case 'don\'t()':
      on = false;
      break;
    default: 
      if (on) {
        const nums = m.matchAll(/(\d{1,3}),(\d{1,3})/g)
        for (const n of nums) {
          total+= parseInt(n[1], 10) * parseInt(n[2], 10) 
        } 
      }
  }

})
console.log(total)