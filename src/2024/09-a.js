const fs = require('fs')

const testLine = fs.readFileSync('09_test.txt').toString()
const prodLine = fs.readFileSync('09_input.txt').toString()

const toBlocks = (data) => {
  const parts = data.split('').map(p => parseInt(p, 10))
  let blocks = []

  let itr = 0
  for (let x = 0; x < parts.length; x += 2) {
    const space = x === parts.length - 1 ? [] : new Array(parts[x + 1]).fill('.', 0, parts[x + 1])
    blocks = [
      ...blocks,
      ...new Array(parts[x]).fill(itr, 0, parts[x]),
      ...space
    ]
    itr++
  }

  return blocks.join('')
}

const swapElements = (array, index1, index2) => {
  // console.log(`swapping ${array[index1]} at ${index1} with ${array[index2]} at ${index2}`)
  array[index1] = array.splice(index2, 1, array[index1])[0]
}

const orderBlocks = (data) => {
  const blocks = data.split('')
  let startIdx = 0
  let endIdx = data.length - 1

  let done = false
  while (!done) {
    // find next .
    let foundEmpty = false
    let foundData = false
    while (!foundEmpty) {
      if (blocks[startIdx] === '.') {
        foundEmpty = true
      } else {
        startIdx++
      }
    }

    // find next number from back
    while (!foundData) {
      if (blocks[endIdx] !== '.') {
        foundData = true
      } else {
        endIdx--
      }
    }

    if (startIdx >= endIdx) {
      done = true
    } else {
      swapElements(blocks, startIdx, endIdx)
    }
  }

  return blocks.join('')
}

const getChecksum = (data) => {
  let sum = 0

  data.split('').forEach((b, idx) => {
    if (b !== '.') {
      sum += parseInt(b, 10) * idx
    }
    if (sum > Number.MAX_SAFE_INTEGER) {
      throw Error('Number.MAX_SAFE_INTEGER')
    }
  })

  return sum
}

const processLine = (l) => {
  const asBlocks = toBlocks(l)
  // console.log(asBlocks)

  let dots = 0
  let nums = 0

  asBlocks.split('').forEach(ab => {
    if (ab === '.') {
      dots++
    } else {
      nums++
    }
  })

  console.log('dots', dots, 'nums', nums)
  dots = 0
  nums = 0
  const orderedBlocks = orderBlocks(asBlocks)
  // console.log(orderedBlocks)

  orderedBlocks.split('').forEach(ab => {
    if (ab === '.') {
      dots++
    } else {
      nums++
    }
  })
  console.log('dots', dots, 'nums', nums)

  return getChecksum(orderedBlocks)
}

if (processLine(testLine) === 1928) {
  console.log('sample data passed')
  console.log(processLine(prodLine))
}

// expect(asBlocks).toEqual('00...111...2...333.44.5555.6666.777.888899')
// expect(orderedBlocks).toEqual('0099811188827773336446555566..............')
// expect(checkSum).toEqual(1928)

// 114626764002
//  88960367801
