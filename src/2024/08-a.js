const fs = require('fs')

const rows = fs.readFileSync('08_input.txt').toString().split('\n')

const antMap = {}

rows.forEach((row, rIdx) => {
  if (!row) {
    return
  }
  row.split('').forEach((col, cIdx) => {
    if (col === '.') {
      return
    }
    if (!antMap[col]) {
      antMap[col] = []
    }
    antMap[col].push({ r: rIdx, c: cIdx })
  })
})

const COLS = rows[0].length
const ROWS = rows.length

const getAntinode = (p1, p2) => {
  const rDiff = p2.r - p1.r
  const cDiff = p2.c - p1.c

  return {
    r: p2.r + rDiff,
    c: p2.c + cDiff
  }
}

const isPointOnMap = (p) => {
  if (p.c >= COLS || p.c < 0 || p.r >= ROWS || p.r < 0) {
    return false
  }
  return true
}

const antinodes = []
Object.keys(antMap).forEach(key => {
  const a = antMap[key]
  for (let x = 0; x < a.length; x++) {
    for (let y = 0; y < a.length; y++) {
      if (x === y) {
        continue
      }
      const antinode = getAntinode(a[x], a[y])
      if (!antinodes.find(a => a.r === antinode.r && a.c === antinode.c) && isPointOnMap(antinode)) {
        antinodes.push(antinode)
      }
    }
  }
})
console.log(antinodes.length)
