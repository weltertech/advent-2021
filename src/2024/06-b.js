const fs = require('fs')

const rows = fs.readFileSync('06_input.txt').toString().split('\n')

const startGrid = []

const moves = {
  '^': (g) => { return { r: g.r - 1, c: g.c } },
  'v': (g) => { return { r: g.r + 1, c: g.c } }, // eslint-disable-line
  '<': (g) => { return { r: g.r, c: g.c - 1 } },
  '>': (g) => { return { r: g.r, c: g.c + 1 } }
}

const next = {
  '^': '>',
  '>': 'v',
  'v': '<', // eslint-disable-line
  '<': '^'
}

let startGuard = { r: -1, c: -1, d: '' }

rows.forEach((row, r) => {
  startGrid.push(row.split('').map((d, c) => {
    if (Object.keys(moves).includes(d)) {
      startGuard = { r, c, d }
      return d
    }
    return d
  }))
})

const logGrid = (g) => {
  g.forEach(r => console.log(r.join('')))
}

const isOnGrid = (g) => {
  if (g.r >= startGrid.length || g.r < 0 || g.c >= startGrid[0].length || g.c < 0) {
    return false
  }
  return true
}
const execute = (sGrid, guard, obstacle) => {
  const path = []
  const grid = sGrid.map(r => { return [...r] }) // use a copy of starting grid
  if (obstacle) {
    grid[obstacle.r][obstacle.c] = 'O'
  }

  let isLoop = false
  let i = 0
  while (isOnGrid(guard)) {
    i++
    const nextPos = moves[guard.d](guard)

    if (grid[nextPos.r] && (grid[nextPos.r][nextPos.c] === '#' || grid[nextPos.r][nextPos.c] === 'O')) { // blocked, rotate
      guard.d = next[guard.d]
    } else {
      // mark current spot
      grid[guard.r][guard.c] = guard.d

      // was I hear before?
      if (path.find(m => {
        return m.r === nextPos.r && m.c === nextPos.c && m.d === guard.d
      })) {
        isLoop = true
        break
      }
      // add to path
      path.push({ r: nextPos.r, c: nextPos.c, d: guard.d })

      // move
      guard = {
        ...guard,
        ...nextPos
      }
    }
    if (i > 20000) {
      console.log('iteration failure!')
      logGrid(grid)
      console.log(guard)
      throw Error('what?')
    }
  }

  return { grid, isLoop }
}

const { grid: knownMovesGrid } = execute(startGrid, { ...startGuard })

let loopPositions = 0
let checked = 0
knownMovesGrid.forEach((row, rIdx) => {
  row.forEach((col, cIdx) => {
    if ('<>^v'.indexOf(col) > -1) {
      // check if it creates a loop
      checked++
      if (checked % 50 === 0) {
        console.log('checked', checked)
      }
      const { isLoop } = execute(startGrid, { ...startGuard }, { r: rIdx, c: cIdx })
      if (isLoop) {
        loopPositions++
      }
    }
  })
})

console.log('checked', checked)
console.log('loopPositions', loopPositions)
