const fs = require('fs')

const array = fs.readFileSync('01_input.txt').toString().split('\n')

const left = {}
const right = {}

for (let i = 0; i < array.length - 1; i++) {
  const [l, r] = array[i].split('   ')

  if (!left[l.trim()]) {
    left[l.trim()] = 0
  }

  left[l.trim()]++

  if (!right[r.trim()]) {
    right[r.trim()] = 0
  }

  right[r.trim()]++
}

let score = 0
Object.keys(left).forEach(leftKey => {
  score += parseInt(leftKey, 10) * parseInt(right[leftKey] || 0, 10)
})

console.log(score)

