const fs = require('fs')

const rows = fs.readFileSync('05_input.txt').toString().split('\n')

const orderRules = []
const updatePages = []

let rulesDone = false
for (let x = 0; x < rows.length; x++) {
  if (rows[x].trim() === '') {
    rulesDone = true
    continue
  }
  if (!rulesDone) {
    orderRules.push(rows[x].split('|').map(v => parseInt(v, 10)))
  } else {
    updatePages.push(rows[x].split(',').map(v => parseInt(v, 10)))
  }
}

let midSum = 0
// for each update...each pair of pages, check if they break a rule
updatePages.forEach(update => {
  let validUpdate = true

  for (let p = 0; p < update.length - 1; p++) {
    for (let r = 0; r < orderRules.length; r++) {
      // rule is violated if backwards
      if (update[p] === orderRules[r][1] && update[p + 1] === orderRules[r][0]) {
        // console.log('RULE BROKEN', update[p], update[p+1], orderRules[r])
        validUpdate = false
      } 
    }
  }
  // get middle number and sum
  if (validUpdate) {
    const midIdx = Math.floor(update.length / 2)
    midSum += update[midIdx]
  }
})

console.log('MID SUM', midSum)