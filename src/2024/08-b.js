const fs = require('fs')

const rows = fs.readFileSync('08_input.txt').toString().split('\n')

const antMap = {}

rows.forEach((row, rIdx) => {
  if (!row) {
    return
  }
  row.split('').forEach((col, cIdx) => {
    if (col === '.') {
      return
    }
    if (!antMap[col]) {
      antMap[col] = []
    }
    antMap[col].push({ r: rIdx, c: cIdx })
  })
})

const COLS = rows[0].length
const ROWS = rows.length

const isPointOnMap = (p) => {
  if (p.c >= COLS || p.c < 0 || p.r >= ROWS || p.r < 0) {
    return false
  }
  return true
}

const getAntinodes = (p1, p2) => {
  const rDiff = p2.r - p1.r
  const cDiff = p2.c - p1.c

  const as = []
  let onMap = true

  let itr = 1
  while (onMap) {
    const next = {
      r: p2.r + (rDiff * itr),
      c: p2.c + (cDiff * itr)
    }

    if (isPointOnMap(next)) {
      as.push(next)
      itr++
    } else {
      onMap = false
    }
  }

  return as
}

function setCharAt (str, index, char) {
  return str.slice(0, index) + char + str.slice(index + 1)
}

const countNodes = (as) => {
  const output = [...rows]
  as.forEach(an => {
    output[an.r] = setCharAt(output[an.r], an.c, '#')
  })

  let count = 0
  output.forEach(oRow => {
    oRow.split('').forEach(chr => {
      if (chr !== '.') {
        count++
      }
    })
  })
  return count
}

const antinodes = []
Object.keys(antMap).forEach(key => {
  const a = antMap[key]
  for (let x = 0; x < a.length; x++) {
    for (let y = 0; y < a.length; y++) {
      if (x === y) {
        continue
      }
      const pAntinodes = getAntinodes(a[x], a[y])
      pAntinodes.forEach(antinode => {
        if (!antinodes.find(a => a.r === antinode.r && a.c === antinode.c)) {
          antinodes.push(antinode)
        }
      })
    }
  }
})

console.log(countNodes(antinodes))
