const fs = require('fs')

const rows = fs.readFileSync('07_input.txt').toString().split('\n')

const equations = []

rows.forEach(row => {
  if (!row) {
    return
  }
  const [answer, values] = row.split(':').map(v => v.trim())
  const nums = values.split(' ').map(v => parseInt(v.trim(), 10))
  equations.push({
    a: parseInt(answer, 10),
    vs: nums
  })
})

const getCombos = (ops, slotCount, combos, curCombo, pos) => {
  ops.forEach(op => {
    curCombo.push(op)
    if (pos === slotCount - 1) {
      combos.push([...curCombo])
    } else {
      getCombos(ops, slotCount, combos, curCombo, pos + 1)
    }
    curCombo.pop()
  })

  return combos
}

const isSolvable = (eq, ops) => {
  const opSlots = eq.vs.length - 1

  let answerMatch = false
  getCombos(ops, opSlots, [], [], 0).forEach(opCombo => {
    let evalued = eq.vs[0] // set starting evaluaed value
    opCombo.forEach((op, opIdx) => {
      const toEval = `${evalued}${op}${eq.vs[opIdx + 1]}`
      evalued = eval(toEval) // eslint-disable-line
    })
    if (evalued === eq.a) {
      answerMatch = true
    }
  })
  return answerMatch
}

const evalAll = (equations, operators) => {
  let solveSums = 0

  equations.forEach(eq => {
    if (isSolvable(eq, operators)) {
      solveSums += eq.a
    }
  })
  return solveSums
}

console.log(evalAll(equations, ['+', '*']))
