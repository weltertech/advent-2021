const start = Date.now()
const fs = require('fs')
const array = fs.readFileSync('01_input.txt').toString().split('\n')

const arr = [
  [1, 1],
  [2, 2],
  [3, 3],
  [4, 4],
  [5, 5],
  [6, 6],
  [7, 7],
  [8, 8],
  [9, 9],
  ['one', 1],
  ['two', 2],
  ['three', 3],
  ['four', 4],
  ['five', 5],
  ['six', 6],
  ['seven', 7],
  ['eight', 8],
  ['nine', 9]
]

let sum = 0
for (let i = 0; i < array.length; i++) { //
  const line = array[i]
  let first, second

  for (let c = 0; c < line.split('').length; c++) {
    for (let a = 0; a < arr.length; a++) {
      if (line.substring(c).startsWith(arr[a][0])) {
        first = arr[a][1]
        break
      }
    }
    if (first) {
      break
    }
  }

  for (let c = 0; c < line.split('').length; c++) {
    for (let a = 0; a < arr.length; a++) {
      if (line.substring(line.split('').length - c - 1).startsWith(arr[a][0])) {
        second = arr[a][1]
        break
      }
    }
    if (second) {
      break
    }
  }

  // console.log(line, first, second)
  sum += parseInt(`${first}${second}`, 10)
}

console.log('count: ', sum)
console.log('time: ', Date.now() - start)
