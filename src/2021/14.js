const fs = require('fs')
const instructionLines = fs.readFileSync('14_input.txt').toString().split('\n')

// const TEMPLATE = 'KBKPHKHHNBCVCHPSPNHF'

const TEMPLATE = 'KBKPHKHHNBCVCHPSPNHF'
// 0
// 'KSBKPHKHHNBCVCHPSPNHF'
// 'KSBKNPHSKHPHNOBCCVCVHPSPNHF'
//  'SBVPHKNHCBOVSPSOSPSSSPSOSBSNSCSPSSSVS

function getInstructions (lines) {
  const instructions = {}
  lines.forEach(line => {
    const [pair, insertChar] = line.split(' -> ')
    instructions[pair] = insertChar
  })
  return instructions
}

function getDeepInstructions (lines) {
  const instructions = {}
  lines.forEach(line => {
    const [pair, insertChar] = line.split(' -> ')
    const [char1, char2] = pair.split('')
    if (!instructions[char1]) {
      instructions[char1] = {}
    }
    instructions[char1][char2] = insertChar
  })
  return instructions
}

function getElementCounts (polymer) {
  const elementCounts = {}
  for (let x = 0; x < polymer.length; x++) {
    const element = polymer.substring(x, x + 1)
    if (!elementCounts[element]) {
      elementCounts[element] = 0
    }
    elementCounts[element]++
  }
  return elementCounts
}

function getMinMaxCounts (elementCounts) {
  const countsArray = Object
    .keys(elementCounts)
    .map((key) => ({ char: key, count: elementCounts[key] }))
    .sort((a, b) => a.count > b.count ? 1 : -1)

  return {
    min: countsArray[0],
    max: countsArray[countsArray.length - 1]
  }
}

function applyInstructions (polymer, instructions) {
// for each linear pair, record insertion points
  const insertions = []
  for (let x = 0; x < polymer.length - 1; x++) {
    const currentPair = polymer.substring(x, x + 2)
    if (instructions[currentPair]) {
      insertions.push({
        char: instructions[currentPair],
        idx: x + 1
      })
    }
  }
  // sort insertions by index
  const sortedInsertions = insertions.sort((a, b) => a.idx > b.idx ? 1 : -1)
  // then insert them all "at once"
  let insertCount = 0
  let updatedPolymer = polymer
  sortedInsertions.forEach(insertion => {
    const position = insertion.idx + insertCount
    updatedPolymer = updatedPolymer.substring(0, position) + insertion.char + updatedPolymer.substring(position, updatedPolymer.length)
    insertCount++
  })
  return updatedPolymer
}

function applyInstructionsFaster (polymer, instructions) {
  // for each linear pair, log insertion points
  let updatedPolymer = polymer
  let x = 0
  while (x < updatedPolymer.length - 1) {
    // console.log('polymer', updatedPolymer)
    const currentPair = updatedPolymer.substring(x, x + 2)
    // console.log('looking at', currentPair)
    if (instructions[currentPair]) {
      // console.log('index', x, 'inserting', instructions[currentPair], 'at', currentPair)
      // insert to updated polymer and increment x appropriately
      // console.log('first', updatedPolymer.substring(0, x + 1), 'insert', instructions[currentPair], 'end', updatedPolymer.substring(x + 1, updatedPolymer.length))
      updatedPolymer = updatedPolymer.substring(0, x + 1) + instructions[currentPair] + updatedPolymer.substring(x + 1, updatedPolymer.length)
      x += 2
    } else {
      x++
    }
  }

  return updatedPolymer
}

const instructions = getInstructions(instructionLines)
const deepInstructions = getDeepInstructions(instructionLines)
// const instructions = {
//   KB: 'C',
//   KC: 'E',
//   CB: 'G'
// }

// KB
// KCB
// KECG

const ITERATION_COUNT = 10

const polymer = TEMPLATE
// let fPolymer = TEMPLATE

const totals = {}

const addChar = (chr) => {
  if (!totals[chr]) {
    totals[chr] = 0
  }
  totals[chr]++
}

const dig = (depth, char1, char2) => {
  if (depth === 18 + 1) {
    // addChar(char1)
    // // addChar(char2)
  } else {
    if (deepInstructions[char1] && deepInstructions[char1][char2]) {
      // console.log(`inserting ${deepInstructions[char1][char2]} between ${char1} and ${char2}`)
      addChar(deepInstructions[char1][char2])
      dig(depth + 1, char1, deepInstructions[char1][char2])
      dig(depth + 1, deepInstructions[char1][char2], char2)
    }
  }
}

// for (let x = 0; x < ITERATION_COUNT; x++) {
//   polymer = applyInstructionsFaster(polymer, instructions)
//   // fPolymer = applyInstructionsFaster(fPolymer, instructions)
//   console.log('after iteration', x + 1, 'length', polymer.length)
//   // console.log('on iteration', x, 'folymer', fPolymer)
// }
const start = Date.now()
for (let x = 0; x < TEMPLATE.length - 1; x++) {
  if (x === 0) {
    addChar(TEMPLATE[x])
  }
  addChar(TEMPLATE[x + 1])
  dig(1, TEMPLATE[x], TEMPLATE[x + 1])
  console.log('finished idx: ', x, 'in', Date.now() - start, 'ms')
}

// console.log('deepInstructions', JSON.stringify(deepInstructions, null, 2))

console.log('i finished!')
console.log('totals', JSON.stringify(totals))

// console.log('polymer', polymer)
// const elementCounts = getElementCounts(polymer)
const minMax = getMinMaxCounts(totals)

// console.log('elementCounts', JSON.stringify(elementCounts, null, 2))
// console.log('minMax', JSON.stringify(minMax))
const answer = minMax.max.count - minMax.min.count
console.log('Answer', answer)
