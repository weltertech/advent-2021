const fs = require('fs')
const _some = require('lodash.some')
const inputLines = fs.readFileSync('12_input.txt').toString().split('\n')

let nodes = []
// organize the data
const addNode = (node, link, nodes) => {
  let existing = nodes.find(n => n.id === node)
  if (!existing) {
    existing = {
      id: node,
      links: [link],
      isStart: node === 'start',
      isEnd: node === 'end',
      isSmall: /^[a-z]+$/.test(node) && node !== 'start' && node !== 'end'
    }
    nodes.push(existing)
  }

  if (!existing.links.includes(link)) {
    existing.links.push(link)
  }
  return nodes
}

inputLines.forEach(line => {
  const [node1, node2] = line.split('-')
  nodes = addNode(node1, node2, nodes)
  nodes = addNode(node2, node1, nodes)
})

const startNode = nodes.find(n => n.id === 'start')

const canVisitSmallAgain = (visited, targetSmallNode) => {
  const smallCounts = {}

  visited.filter(v => v.isSmall).forEach(v => {
    if (!smallCounts[v.id]) {
      smallCounts[v.id] = 0
    }
    smallCounts[v.id] = smallCounts[v.id] + 1
  })

  const targetVisited = !!smallCounts[targetSmallNode.id]
  const smallWithTwo = _some(Object.values(smallCounts), value => value >= 2)

  if (smallWithTwo) {
    if (targetVisited) {
      return false // cannot have ANOTHER small cave with two
    } else {
      return true // this will be this first small cave with two
    }
  } else {
    return true
  }
}

let paths = 0
const navigate = (node, visited) => {
  visited.push(node)
  node.links.forEach(link => {
    const linkNode = nodes.find(n => n.id === link)
    // if small and already visited, don't bother
    if (linkNode.isSmall) {
      if (!canVisitSmallAgain(visited, linkNode)) {
        // do nothing - can't visit small cave again
        return
      }
    }

    if (linkNode.isStart) {
      // don't go back to start
      return
    }

    if (linkNode.isEnd) {
      paths++
    } else {
      navigate(linkNode, visited)
    }
    // if end, tally unique path
  })
  visited.pop()
}

navigate(startNode, [])

console.log('paths', paths)
