const fs = require('fs')
const inputLines = fs.readFileSync('05_input.txt').toString().split('\n')

const segments = []

const markMap = (segments) => {
  const markedMap = []

  segments.forEach(({ x1, x2, y1, y2 }) => {
    if (x1 === x2) {
      let start = y1
      let end = y2

      if (y1 > y2) {
        start = y2
        end = y1
      }

      for (let y = start; y <= end; y++) {
        if (!markedMap[y]) {
          markedMap[y] = []
        }
        if (!markedMap[y][x1]) {
          markedMap[y][x1] = 0
        }
        markedMap[y][x1] = markedMap[y][x1] + 1
      }
    } else if (y1 === y2) {
      // horizontal line when Y doesnt change
      let start = x1
      let end = x2

      if (x1 > x2) {
        start = x2
        end = x1
      }

      for (let x = start; x <= end; x++) {
        if (!markedMap[y1]) {
          markedMap[y1] = []
        }
        if (!markedMap[y1][x]) {
          markedMap[y1][x] = 0
        }
        markedMap[y1][x] = markedMap[y1][x] + 1
      }
    } else { // DIAGONAL!
      // which point is on the left?
      let p1x = x1
      let p2x = x2
      let p1y = y1
      let p2y = y2

      if (x1 > x2) {
        // swap the points so we can go left to right
        p1x = x2
        p1y = y2
        p2x = x1
        p2y = y1
      }

      const increase = (val) => {
        const newval = val + 1
        return newval
      }
      const decrease = (val) => {
        const newval = val - 1
        return newval
      }
      const yIncrement = p1y < p2y ? increase : decrease
      let y = p1y

      for (let x = p1x; x <= p2x; x++) {
        if (!markedMap[y]) {
          markedMap[y] = []
        }
        if (!markedMap[y][x]) {
          markedMap[y][x] = 0
        }
        markedMap[y][x] = markedMap[y][x] + 1
        y = yIncrement(y)
      }
    }
  })

  return markedMap
}

inputLines.forEach(line => {
  const [point1, point2] = line.split('->')
  const [x1, y1] = point1.split(',')
  const [x2, y2] = point2.split(',')
  segments.push({
    x1: parseInt(x1.trim(), 10),
    y1: parseInt(y1.trim(), 10),
    x2: parseInt(x2.trim(), 10),
    y2: parseInt(y2.trim(), 10)
  })
})

const markedMap = markMap(segments)

let newIntersectCount = 0
for (let y = 0; y < markedMap.length; y++) {
  const line = markedMap[y]
  if (line) {
    for (let x = 0; x < line.length; x++) {
      if (line[x] > 1) {
        newIntersectCount++
      }
    }
  }
}

// console.log(JSON.stringify(markedMap))
console.log('NEW Intersect Count', newIntersectCount)
