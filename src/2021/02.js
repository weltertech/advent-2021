const fs = require('fs')
const array = fs.readFileSync('02_input.txt').toString().split('\n')

let h = 0
let d = 0
let aim = 0
for (let i = 0; i < array.length; i++) {
  const [direction, amount] = array[i].split(' ')
  const x = parseInt(amount, 10)
  switch (direction) {
    case 'up':
      aim -= x
      break
    case 'down':
      aim += x
      break
    case 'forward':
      h += x
      d += (aim * x)
      break
    default:
      console.log('misunderstood direction: ', direction)
  }
}

console.log('h: ', h)
console.log('d: ', d)
console.log('answer: ', h * d)
