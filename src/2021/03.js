const fs = require('fs')
const array = fs.readFileSync('03_input.txt').toString().split('\n')

const getValueAtPosition = (string, position) => {
  const charsArr = string.split('')
  const val = parseInt(charsArr[position], 10)
  return val
}

const positionEquals = (string, position, value) => {
  const charsArr = string.split('')
  const keep = charsArr[position] === value
  console.log(string, ' val: ', charsArr[position], '  keep? ', keep)

  return keep
}

// probably a MUCH cleaner way to do this
const getOxygenKeeper = (bits) => {
  let keepersStartWith = '1'
  if (bits[0] > bits[1]) {
    keepersStartWith = '0'
  } else if (bits[0] === bits[1]) {
    keepersStartWith = '1'
  } else {
    keepersStartWith = '1'
  }
  return keepersStartWith
}

const getO2Keeper = (bits) => {
  let keepersStartWith = '1'
  if (bits[0] > bits[1]) {
    keepersStartWith = '1'
  } else if (bits[0] === bits[1]) {
    keepersStartWith = '0'
  } else {
    keepersStartWith = '0'
  }
  return keepersStartWith
}

const reduce = (array, index, getKeeperFn, instance) => {
  console.log('=====================================================')
  console.log('index: ', index, '  instance: ', instance)

  const bits = [0, 0]
  for (let lineIdx = 0; lineIdx < array.length; lineIdx++) {
    const val = getValueAtPosition(array[lineIdx], index)
    bits[val]++
  }

  const keepersStartWith = getKeeperFn(bits)
  const replaceKeepers = []
  for (let x = 0; x < array.length; x++) {
    if (positionEquals(array[x], index, keepersStartWith)) {
      replaceKeepers.push(array[x])
    }
  }

  return [...replaceKeepers]
}

const getVal = (data, getKeeperFn, instance) => {
  let done = false
  let keepers = []
  let position = -1

  keepers = [...data]
  while (!done) {
    position++
    keepers = reduce(keepers, position, getKeeperFn, instance)
    if (keepers.length <= 1) {
      done = true
    }
  }

  return parseInt(keepers[0], 2)
}

const oxyVal = getVal(array, getOxygenKeeper, 'Oxygen')
const o2Val = getVal(array, getO2Keeper, 'O2')

console.log('oxyVal', oxyVal)
console.log('o2Val', o2Val)
console.log('answer', oxyVal * o2Val)
