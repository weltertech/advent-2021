const fs = require('fs')
const _map = require('lodash.map')
const inputLines = fs.readFileSync('15_input.txt').toString().split('\n')
const Graph = require('node-dijkstra')

const START_NODE = '0-0'
const NEW_END = '499-499'

const data = _map(inputLines, inputLine => {
  const nums = inputLine.trim().split('')
  return _map(nums, num => parseInt(num, 10))
})

const getLinkNode = (x, y, data) => {
  if (data[x] && data[x][y]) {
    return {
      [`${x}-${y}`]: data[x][y]
    }
  } else {
    return {}
  }
}

let nodeCount = 0
const getExpandedData = (data) => {
  const expandedData = []

  function addNode (x, y, val) {
    nodeCount++
    if (!expandedData[x]) {
      expandedData[x] = []
    }
    expandedData[x][y] = val
  }

  function getWrappedVal (val) {
    if (val < 10) {
      return val
    }
    return val - 9
  }

  for (let x = 0; x < data.length; x++) {
    for (let y = 0; y < data[x].length; y++) {
      // starting node
      const startingVal = data[x][y]
      const len = data[x].length
      addNode(x, y, startingVal)
      for (let x2 = 0; x2 < 5; x2++) {
        for (let y2 = 0; y2 < 5; y2++) {
          addNode(x + (x2 * len), y + (y2 * len), getWrappedVal(startingVal + x2 + y2))
        }
      }
    }
  }
  return expandedData
}

const getGraphRoute = (expandedData) => {
  const route = new Graph()
  for (let x = 0; x < expandedData.length; x++) {
    for (let y = 0; y < expandedData[x].length; y++) {
      const up = getLinkNode(x - 1, y, expandedData)
      const down = getLinkNode(x + 1, y, expandedData)
      const left = getLinkNode(x, y - 1, expandedData)
      const right = getLinkNode(x, y + 1, expandedData)

      console.log('adding route node', `${x}-${y}`)
      route.addNode(`${x}-${y}`, {
        ...up, ...down, ...left, ...right
      })
    }
  }
  return route
}

const printData = (data) => {
  const ll = data[0].length
  for (let x = 0; x < ll; x++) {
    const vals = []
    for (let y = 0; y < ll; y++) {
      vals.push(data[x] && data[x][y] ? data[x][y] : '-')
    }
    console.log(vals.join(' '))
  }
}

const expandedData = getExpandedData(data)
const route = getGraphRoute(expandedData)
const path = route.path(START_NODE, NEW_END, { cost: true })
console.log('node count', nodeCount)
console.log('path', path)
