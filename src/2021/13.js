const fs = require('fs')
// const _some = require('lodash.some')
const coordsInputLines = fs.readFileSync('13_input1.txt').toString().split('\n')
const foldInputLines = fs.readFileSync('13_input2.txt').toString().split('\n')

function parseCoordsInput (cInputLines) {
  const coords = []
  cInputLines.forEach(line => {
    const [strX, strY] = line.split(',')
    const x = parseInt(strX, 10)
    const y = parseInt(strY, 10)
    coords.push({ x, y })
  })

  return coords
}

function parseFoldsInput (fInputLines) {
  const folds = []
  fInputLines.forEach(line => {
    const [, keep] = line.split('fold along ')
    const [axis, strIdx] = keep.split('=')
    const index = parseInt(strIdx, 10)
    folds.push({ axis, index })
  })
  return folds
}

function pushCoord (coords, newCoord) {
  const found = coords.find(c => {
    return c.x === newCoord.x && c.y === newCoord.y
  })
  if (!found) {
    coords.push(newCoord)
  }
  return coords
}

function singleXFold (c, index) {
  let newCoord
  if (c.x >= index) {
    newCoord = {
      x: index - (c.x - index),
      y: c.y
    }
  } else {
    newCoord = {
      x: c.x,
      y: c.y
    }
  }

  return newCoord
}

function singleYFold (c, index) {
  let newCoord
  if (c.y >= index) {
    newCoord = {
      x: c.x,
      y: index - (c.y - index)
    }
  } else {
    newCoord = {
      x: c.x,
      y: c.y
    }
  }

  return newCoord
}

function joinLine (lineArr) {
  let output = ''
  for (let x = 0; x < lineArr.length; x++) {
    if (lineArr[x]) {
      output += 'X'
    } else {
      output += '.'
    }
  }
  return output
}

function debugOutput (coords) {
  const output = []
  coords.forEach(c => {
    if (!output[c.y]) {
      output[c.y] = []
    }
    output[c.y][c.x] = 'X'
  })

  for (let x = 0; x < output.length; x++) {
    if (!output[x]) {
      console.log(`${x < 10 ? ` ${x}` : x}: -----------------------`)
    } else {
      console.log(`${x < 10 ? ` ${x}` : x}: ${joinLine(output[x])}`)
    }
  }
}

function doFoldY (coords, index) {
  let newCoords = []
  coords.forEach(c => {
    if (c.y !== index) {
      newCoords = pushCoord(newCoords, singleYFold(c, index))
    }
  })
  return newCoords
}

function doFoldX (coords, index) {
  let newCoords = []
  coords.forEach(c => {
    if (c.x !== index) {
      newCoords = pushCoord(newCoords, singleXFold(c, index))
    } else {
      console.log('point on fold, skip it')
    }
  })
  return newCoords
}

function doFold (coords, fold) {
  if (fold.axis === 'x') {
    return doFoldX(coords, fold.index)
  } else {
    return doFoldY(coords, fold.index)
  }
}

const coords = parseCoordsInput(coordsInputLines)
const folds = parseFoldsInput(foldInputLines)

let solve = coords
folds.forEach(fold => {
  solve = doFold(solve, fold)
})

console.log(solve.length)
debugOutput(solve)
