const fs = require('fs')
const array = fs.readFileSync('01_input.txt').toString().split('\n')

let prev
let count = 0
for (let i = 0; i < array.length - 2; i++) {
  const current = parseInt(array[i], 10) + parseInt(array[i + 1], 10) + parseInt(array[i + 2], 10)
  if (!prev) {
    prev = current
    continue
  }
  if (current > prev) {
    count++
  }
  prev = current
}

console.log('count: ', count)
