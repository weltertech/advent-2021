const fs = require('fs')
const inputLines = fs.readFileSync('16_input.txt').toString().split('\n')

const getBinaryFromHex = (line) => {
  const binary = []

  line.split('').forEach(char => {
    binary.push(parseInt(char, 16).toString(2).padStart(4, '0'))
  })

  return binary.join('')
}

const popChars = (str, count) => {
  return {
    popped: str.substring(0, count),
    rest: str.substring(count, str.length - count)
  }
}

const getLiteralValue = (transmission) => {
  // get 5 bits at a time until it starts with 0
  let done = false
  let value = ''
  let t = transmission
  while (!done) {
    const { popped: val, rest } = popChars(t, 5)
    t = rest

    value += val
    if (val.startsWith('0')) {
      done = true
    }
  }

  return {
    rest: t,
    value
  }
}

const getNextPacket = (transmission) => {
  // get version
  const { popped: version, rest: afterVersion } = popChars(transmission, 3)
  // get id type
  const { popped: idTypeBinary, rest: afterType } = popChars(afterVersion, 3)

  const idType = parseInt(idTypeBinary, 2)
  console.log('idType', idTypeBinary)

  switch (idType) {
    case 4: // literal value
      return {
        ...getLiteralValue(afterType),
        version,
        type: 'literal value'
      }
    case 6: // packet is an operator
      return {
        version,
        type: 'operator'
      }
    case 3: // packet is an operator
      return {
        version,
        type: 'operator'
      }
    default: return { error: 'packet id type not found' }
  }
}

const b = getBinaryFromHex(inputLines[0])
console.log(b)
let x = 0
let rest = b
while (x < 1) {
  const packet = getNextPacket(rest)
  console.log(packet)
  rest = packet.rest
  x++
}
