const fs = require('fs')
const _map = require('lodash.map')
const inputLines = fs.readFileSync('06_input.txt').toString().split('\n')

const input = _map(inputLines[0].split(','), strNum => parseInt(strNum, 10))

const allFish = [...input]

let counts = [0, 0, 0, 0, 0, 0, 0, 0, 0]

for (let x = 0; x < allFish.length; x++) {
  const currentFish = allFish[x]
  counts[currentFish] = counts[currentFish] + 1
}

const DAYS = 256

for (let day = 0; day < DAYS; day++) {
  const newCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  const newFish = counts[0] // the zeros get added
  for (let i = 9; i > -1; i--) {
    if (i > 0 && i < 9) {
      newCounts[i - 1] = counts[i]
    }
    if (i === 0) {
      newCounts[8] = newFish
      newCounts[6] = newCounts[6] + newFish
    }
  }
  counts = newCounts
}

let sum = 0
for (let i = 0; i <= 8; i++) {
  sum += counts[i]
}

console.log('fish count', sum)
