const fs = require('fs')
const _map = require('lodash.map')
const inputLines = fs.readFileSync('11_input.txt').toString().split('\n')

const data = _map(inputLines, inputLine => {
  const nums = inputLine.trim().split('')
  return _map(nums, num => parseInt(num, 10))
})

const STEPS = 10000

const octopi = [...data]

const incrementAll = () => {
  for (let x = 0; x < octopi.length; x++) {
    for (let y = 0; y < octopi[x].length; y++) {
      octopi[x][y] = octopi[x][y] + 1
    }
  }
}

let flashed = []

const incrementFromFlash = (x, y) => {
  if (x < 0 || y < 0 || x > 9 || y > 9) {
    return // invalid point, do nothing
  }
  octopi[x][y] = octopi[x][y] + 1

  flashTest(x, y)
}

const flashTest = (x, y) => {
  if (x < 0 || y < 0 || x > 9 || y > 9) {
    return // invalid point, do nothing
  }

  // if greater than 9 and hasn't already flashed, flash!
  if (octopi[x][y] > 9 && !flashed.find(fl => fl.x === x && fl.y === y)) {
    // flash!
    flashed.push({ x, y })
    // increase all around
    incrementFromFlash(x - 1, y - 1)
    incrementFromFlash(x, y - 1)
    incrementFromFlash(x + 1, y - 1)

    incrementFromFlash(x - 1, y)
    incrementFromFlash(x + 1, y)

    incrementFromFlash(x - 1, y + 1)
    incrementFromFlash(x, y + 1)
    incrementFromFlash(x + 1, y + 1)
  }
}

const setFlashedToZero = () => {
  for (let x = 0; x < octopi.length; x++) {
    for (let y = 0; y < octopi[x].length; y++) {
      if (octopi[x][y] > 9) {
        octopi[x][y] = 0
      }
    }
  }
  flashed = []
}

let flashTotal = 0
for (let x = 0; x < STEPS; x++) {
  incrementAll()
  for (let x = 0; x < octopi.length; x++) {
    for (let y = 0; y < octopi[x].length; y++) {
      flashTest(x, y)
    }
  }
  if (flashed.length === 100) {
    console.log('sync flash at step ', x + 1)
    break
  }
  flashTotal += flashed.length
  setFlashedToZero()
}

console.log('flashes', flashTotal)
