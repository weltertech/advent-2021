const fs = require('fs')
const inputLines = fs.readFileSync('10_input.txt').toString().split('\n')

const pairs = [
  { open: '{', close: '}', points: 1197, p: 3 },
  { open: '<', close: '>', points: 25137, p: 4 },
  { open: '[', close: ']', points: 57, p: 2 },
  { open: '(', close: ')', points: 3, p: 1 }
]

const getExpectedPair = (char) => {
  return pairs.find(p => p.close === char) || pairs.find(p => p.open === char)
}

const lineScores = []

inputLines.forEach(line => {
  const lineStack = []
  let isCorrupt = false
  const chars = line.split('')
  for (let i = 0; i < chars.length; i++) {
    const char = chars[i]
    const expectedPair = getExpectedPair(char)
    if (char === expectedPair.open) {
      lineStack.push(char)
    } else { // its a close char
    // compare to last on stack stack, shoiuld find matching open char
      if (lineStack[lineStack.length - 1] === expectedPair.open) {
        lineStack.pop()
      } else {
      // if not, illegal char
        isCorrupt = true
        break
      }
    }
  }

  if (!isCorrupt) {
    let score = 0
    // pop each char from the array
    while (lineStack.length > 0) {
      // it should be an open char, so find the closer
      const pair = getExpectedPair(lineStack.pop())

      score = (score * 5) + pair.p
    }
    lineScores.push(score)
  }
})

const sorted = lineScores.sort((a, b) => a > b ? 1 : -1)
const mid = sorted[Math.round((sorted.length - 1) / 2)]

console.log('mid', mid)
