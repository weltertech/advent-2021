const fs = require('fs')
const _map = require('lodash.map')
const array = fs.readFileSync('04_input.txt').toString().split('\n')

const isPulledLine = (string) => {
  return string.split(',').length > 25
}

const isBlankLine = (string) => {
  return string.length === 0
}

const isBoardLine = (string) => {
  return string.split(/\s+/).length > 1
}

const getWinnerScore = (board, pulledNumbers, lastNumber) => {
  let score = 0
  board.forEach((line, lineIdx) => {
    line.forEach((number, numberIdx) => {
      if (!pulledNumbers.includes(number)) {
        score += number
      } else {
        console.log('number was marked, not adding to sum')
      }
    })
  })

  return score * lastNumber
}

const parseInput = (array) => {
  let pulled = []
  const boards = []
  const marked = []
  let boardIndex = -1
  let boardLine = -1

  for (let x = 0; x < array.length; x++) {
    const line = array[x]
    switch (true) {
      case isPulledLine(line):
        pulled = _map(line.split(','), strNum => parseInt(strNum.trim(), 10))
        break
      case isBlankLine(line):
        // start next board
        boardIndex++
        boardLine = 0

        boards[boardIndex] = [[], [], [], [], []]
        marked[boardIndex] = [[], [], [], [], []] // make empty boards for marking up
        break
      case isBoardLine(line):
        // yuck...filter due to an unexpected null in board array
        boards[boardIndex][boardLine] = _map(line.split(/\s+/).filter(item => item !== ''), strNum => parseInt(strNum.trim(), 10))
        marked[boardIndex][boardLine] = [false, false, false, false, false] // unmarked line
        boardLine++
        break
    }
  }
  return { pulled, marked, boards }
}

const markBoards = (pulledNumber, boards, marked) => {
  boards.forEach((board, boardIdx) => {
    board.forEach((line, lineIdx) => {
      line.forEach((number, numberIdx) => {
        if (number === pulledNumber) {
          marked[boardIdx][lineIdx][numberIdx] = true
        }
      })
    })
  })
}

const boardIsWinner = (markedBoard) => {
  let winner
  // check rows
  const isColWinner = [true, true, true, true, true]
  markedBoard.forEach((line, lineIdx) => {
    let isLineWinner = true
    line.forEach((mark, numberIdx) => {
      if (!mark) {
        isLineWinner = false
        isColWinner[numberIdx] = false
      }
    })

    if (isLineWinner) {
      winner = 'LINE'
    }
  })

  // check cols
  isColWinner.forEach((col, idx) => {
    if (col === true) {
      winner = `COL-${idx}`
    }
  })

  // check diags
  // forward slash diag
  let isDiagOneWinner = true
  for (let x = 0; x < 5; x++) {
    for (let y = 4; y >= 0; y--) {
      if (!markedBoard[x][y]) {
        isDiagOneWinner = false
      }
    }
  }
  let isDiagTwoWinner = true
  for (let x = 4; x >= 0; x--) {
    for (let y = 0; y < 5; y++) {
      if (!markedBoard[x][y]) {
        isDiagTwoWinner = false
      }
    }
  }

  if (isDiagOneWinner) {
    winner = 'DIAG-B'
  }
  if (isDiagTwoWinner) {
    winner = 'DIAG-F'
  }
  return winner
}

const getNewWinnerIndexes = (boards, marked, winningBoards) => {
  const newWinners = []
  for (let x = 0; x < boards.length; x++) {
    const winLabel = boardIsWinner(marked[x])
    if (winLabel && !winningBoards[x]) {
      newWinners.push({ winIndex: x, winLabel })
    }
  }
  return newWinners
}

const { pulled, boards, marked } = parseInput(array)

let pulledIdx = -1
const called = []

const winnerList = []
let winnerCount = 0
boards.forEach((board, boardIdx) => {
  winnerList[boardIdx] = undefined
})

let foundLastWinner = false
let lastWinnerIdx = -1
let pulledToWinner = []
let lastNumberToWinner = -1
for (let p = 0; p < pulled.length; p++) {
  pulledIdx++
  called.push(pulled[pulledIdx])

  markBoards(pulled[pulledIdx], boards, marked)

  const newWinners = getNewWinnerIndexes(boards, marked, winnerList)

  newWinners.forEach(({ winIndex, winLabel }) => {
    pulledToWinner = [...called]
    lastNumberToWinner = pulled[pulledIdx]
    lastWinnerIdx = winIndex
    winnerCount++
    winnerList[winIndex] = winLabel

    if (winnerCount === boards.length) {
      foundLastWinner = true
      const winningScore = getWinnerScore(boards[lastWinnerIdx], pulledToWinner, lastNumberToWinner)
      console.log('Pulled Numbers: ', JSON.stringify(called))
      console.log('Winning Board: ', JSON.stringify(boards[winIndex], null, 2))
      console.log('WINNING SCORE: ', winningScore)
    }
  })

  if (foundLastWinner) {
    console.log('FOUND LAST WINNER')
    break
  }
}
