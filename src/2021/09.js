const fs = require('fs')
const _map = require('lodash.map')
const inputLines = fs.readFileSync('09_input.txt').toString().split('\n')

const data = _map(inputLines, inputLine => {
  const nums = inputLine.trim().split('')
  return _map(nums, num => parseInt(num, 10))
})

const MAX = Number.MAX_SAFE_INTEGER
console.log(data)
const isLowest = (x, a = MAX, b = MAX, c = MAX, d = MAX) => {
  return x < a && x < b && x < c && x < d
}

let checkedPoints = []

const checked = (x, y) => {
  return checkedPoints.find(cp => cp.x === x && cp.y === y)
}

const findBasinSpots = (x, y, previousDepth) => {
  if (!data[x] || data[x][y] === undefined) { // looking out of bounds, return 0
    return 0
  }

  if (checked(x, y)) { // already checked this spot, return 0
    return 0
  }

  let basinCount = 0

  const currentDepth = data[x][y]
  checkedPoints.push({ x, y })

  if ((previousDepth !== -1 || currentDepth >= previousDepth) && currentDepth !== 9) {
    basinCount++
    basinCount += findBasinSpots(x + 1, y, currentDepth)
    basinCount += findBasinSpots(x, y + 1, currentDepth)
    basinCount += findBasinSpots(x - 1, y, currentDepth)
    basinCount += findBasinSpots(x, y - 1, currentDepth)
  }

  return basinCount
}

const basinSizes = []

for (let x = 0; x < data.length; x++) {
  for (let y = 0; y < data[x].length; y++) {
    const val = data[x][y]
    const a = data[x - 1] ? data[x - 1][y] : MAX
    const b = data[x][y - 1]
    const c = data[x + 1] ? data[x + 1][y] : MAX
    const d = data[x][y + 1]

    if (isLowest(val, a, b, c, d)) {
      checkedPoints = []
      const basinSize = findBasinSpots(x, y, -1)
      basinSizes.push({ size: basinSize, x, y })
    }
  }
}

const sortedSizes = basinSizes.sort((a, b) => a.size < b.size ? 1 : -1)

console.log(sortedSizes[0].size * sortedSizes[1].size * sortedSizes[2].size)
