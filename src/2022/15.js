const fs = require('fs')
const example = fs.readFileSync('15_example.txt').toString().split('\n')
const full = fs.readFileSync('15.txt').toString().split('\n')

let sensors = []

const addRangeToRow = (groups, n) => {
  console.log('adding', n, 'to', groups)
  const overLap = []
  const noOverLap = []

  for (let gi = 0; gi < groups.length; gi++) {
    const g = groups[gi]
    // if envelops, throw it out
    if (g.low <= n.low && g.high >= n.high) {
      // throw this one out and return previous groups
      console.log('existing envolopes new, throw it out')
      return groups
    } else if (n.low > g.high || n.high < g.low) {
      noOverLap.push(g)
    } else {
      overLap.push(g)
    }
  }

  console.log('groups with overlap', overLap)
  if (overLap.length > 0) {
    // find high & low of overlap, make one
    let low = Infinity
    let high = -Infinity

    overLap.push(n)

    overLap.forEach(item => {
      if (item.low < low) {
        low = item.low
      }
      if (item.high > high) {
        high = item.high
      }
    })

    console.log(' overlap combined to ', { low, high })
    return [
      ...noOverLap,
      { low, high }
    ]
  }
  return [
    ...noOverLap,
    n
  ]
}

const NEW_ROW = () => ({
  BS: [],
  groups: []
})
const rows = {}
//    Sensor at x=2, y=18: closest beacon is at x=-2, y=15

let lowX = Infinity
let lowY = Infinity
let highX = -Infinity
let highY = -Infinity
let buffer = 0

const cabDistance = (one, two) => {
  return Math.abs(one.x - two.x) + Math.abs(one.y - two.y)
}

const addToGrid = (pos, char) => {
  if (char === 'B' || char === 'S') {
    if (pos.x < lowX) {
      lowX = pos.x
    }
    if (pos.x > highX) {
      highX = pos.x
    }
    if (pos.y < lowY) {
      lowY = pos.y
    }
    if (pos.y > highY) {
      highY = pos.y
    }
    if (!rows[pos.x]) {
      rows[pos.x] = NEW_ROW()
    }
    rows[pos.x].BS.push({ ...pos, t: char })
  }
}

const loadDataFromLines = (lines) => {
  lines.forEach(l => {
    const [sp, bp] = l.split(':')
    const [,, sxp, syp] = sp.split(' ')
    const [,,,,, bxp, byp] = bp.split(' ')

    const sensor = {
      x: parseInt(sxp.replace(',', '').split('=')[1], 10) + buffer,
      y: parseInt(syp.split('=')[1], 10) + buffer
    }
    addToGrid(sensor, 'S')

    const beacon = {
      x: parseInt(bxp.replace(',', '').split('=')[1], 10) + buffer,
      y: parseInt(byp.split('=')[1], 10) + buffer
    }
    addToGrid(beacon, 'B')

    const distance = cabDistance(sensor, beacon)

    sensors.push({
      sensor,
      beacon,
      distance
    })

    console.log(sensor, beacon)
  })
}

const markDeadSpots = (iterations) => {
  const count = 0
  sensors.forEach(pair => {
    if (count >= iterations) {
      throw Error('break')
    }
    console.log('marking for', pair)
    const s = pair.sensor
    // console.log('markingDead sensor', pair)
    // mark up the map with dead spots distance - 1 from sensor

    let row = 0
    let width = -1
    const startX = s.x - pair.distance > lowX ? s.x - pair.distance : lowX
    const endX = s.x + pair.distance < highX ? s.x + pair.distance : highX
    for (let x = startX; x <= endX; x++) { // this is correct rows
      if (row <= pair.distance) {
        width++
      } else {
        width--
      }
      const yStart = s.y - width >= lowY ? s.y - width : lowY
      const yEnd = s.y + width <= highY ? s.y + width : highY

      if (!rows[x]) {
        rows[x] = NEW_ROW()
      }

      rows[x].groups = addRangeToRow([...rows[x].groups], { low: yStart, high: yEnd })
      console.log('  new groups', rows[x].groups)
      row++
    }
  })
}
// const test = ['Sensor at x=2, y=18: closest beacon is at x=-2, y=15']
const findDeadInRow = (row) => {
  let count = 0
  for (let x = lowX; x <= highX; x++) {
    // is row in groups and not a B or S
    const inGroup = rows[x].groups.some(g => {
      return g.low <= row && g.high >= row
    })
    const inBS = rows[x].BS.some(bs => {
      return bs.x === x && bs.y === row
    })

    if (!inGroup || inBS) {
      console.log('COL', x, inGroup, inBS)
    }
    if (inGroup && !inBS) {
      count++
    }
  }
  return count
}

const drawGrid = () => {
  console.log(' ')
  const grid = []
  // fill
  for (let y = lowY; y <= highY; y++) {
    if (!grid[y]) {
      grid[y] = []
    }
    for (let x = lowX; x <= highX; x++) {
      grid[y][x] = '.'
    }
  }

  Object.keys(rows).forEach(rid => {
    rows[rid].groups.forEach(g => {
      for (let x = g.low; x <= g.high; g++) {
        if (!grid[x]) {
          grid[x] = []
        }
        grid[x][rid] = '#'
      }
    })
    rows[rid].BS.forEach(bs => {
      grid[bs.y][bs.x] = bs.t
    })
  })

  for (let y = lowY; y <= highY; y++) {
    console.log(grid[y].join(''))
  }
  console.log(' ')
}

// loadDataFromLines(example)
// // adjust buffer
// buffer = lowX > lowY ? Math.abs(lowX) : Math.abs(lowY)
buffer = 100
// lowX = -100
// highX = 100
// lowY = -100
// highY = 100
sensors = []
// start over with new buffer
loadDataFromLines(example)

console.log('DATA LOADED')

// drawGrid()
try {
  markDeadSpots(10000)
} catch (e) {

}

console.log('rows', JSON.stringify(rows, null, 2))

// console.log('rows', JSON.stringify(rows, null, 2))
// console.log('ranges marked, finding dead in row')
// console.log('dead in row', findDeadInRow(10))
console.log('x', lowX, highX)
console.log('y', lowY, highY)
drawGrid()
