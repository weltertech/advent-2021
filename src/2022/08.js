const fs = require('fs')
const lines = fs.readFileSync('08.txt').toString().split('\n')

// load up the 2d array
const trees = []

lines.forEach((line, r_idx) => { // eslint-disable-line
  trees[r_idx] = []
  line.split('').forEach((tree, c_idx) => { // eslint-disable-line
    trees[r_idx][c_idx] = parseInt(tree, 10)
  })
})

const visible = []

const alreadyFound = (list, r, c) => {
  return list.find(vtree => vtree.r === r && vtree.c === c)
}

const rows = trees.length
const cols = trees[0].length

for (let r = 0; r < rows; r++) {
  let tallest = -1
  for (let c = 0; c < cols; c++) {
    if (trees[r][c] > tallest) {
      if (!alreadyFound(visible, r, c)) {
        visible.push({ r, c })
      }
      tallest = trees[r][c]
    }
  }
}

for (let r = 0; r < rows; r++) {
  let tallest = -1
  for (let c = cols - 1; c > -1; c--) {
    if (trees[r][c] > tallest) {
      if (!alreadyFound(visible, r, c)) {
        visible.push({ r, c })
      }
      tallest = trees[r][c]
    }
  }
}

for (let c = 0; c < cols; c++) {
  let tallest = -1
  for (let r = rows - 1; r > -1; r--) {
    if (trees[r][c] > tallest) {
      if (!alreadyFound(visible, r, c)) {
        visible.push({ r, c })
      }
      tallest = trees[r][c]
    }
  }
}

for (let c = 0; c < cols; c++) {
  let tallest = -1
  for (let r = 0; r < rows; r++) {
    if (trees[r][c] > tallest) {
      if (!alreadyFound(visible, r, c)) {
        visible.push({ r, c })
      }
      tallest = trees[r][c]
    }
  }
}

// decrease C
const lookUp = (r, c) => {
  const start = trees[r][c]
  let steps = 0
  for (let C = c - 1; C > -1; C--) {
    steps++
    if (trees[r][C] >= start) {
      break
    }
  }
  return steps
}

// increase C
const lookDown = (r, c) => {
  const start = trees[r][c]
  let steps = 0
  for (let C = c + 1; C < cols; C++) {
    steps++
    if (trees[r][C] >= start) {
      break
    }
  }
  return steps
}

// decrease R
const lookLeft = (r, c) => {
  const start = trees[r][c]
  let steps = 0
  for (let R = r - 1; R > -1; R--) {
    steps++
    if (trees[R][c] >= start) {
      break
    }
  }
  return steps
}

// increaseR
const lookRight = (r, c) => {
  const start = trees[r][c]
  let steps = 0
  for (let R = r + 1; R < rows; R++) {
    steps++
    if (trees[R][c] >= start) {
      break
    }
  }
  return steps
}

let highScore = -1
for (let r = 0; r < rows; r++) {
  for (let c = 0; c < cols; c++) {
    const up = lookUp(r, c)
    const down = lookDown(r, c)
    const left = lookLeft(r, c)
    const right = lookRight(r, c)
    const total = up * down * left * right
    if (total > highScore) {
      highScore = total
    }
  }
}

console.log('visible', visible.length)
console.log('high score', highScore)
