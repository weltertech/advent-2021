const fs = require('fs')
const array = fs.readFileSync('02.txt').toString().split('\n')

const selPoints = { X: 1, Y: 2, Z: 3 }
const resPoints = { W: 6, L: 0, D: 3 }
const resPoints2 = { X: 0, Y: 3, Z: 6 }

const rules = {
  A: { X: 'D', Y: 'W', Z: 'L' }, // Rock     / Rock / Paper / Scissors
  B: { X: 'L', Y: 'D', Z: 'W' }, // Paper    / Rock / Paper / Scissors
  C: { X: 'W', Y: 'L', Z: 'D' } //  Scissors / Rock / Paper / Scissors
}

const rules2 = {
  A: { X: 3, Y: 1, Z: 2 }, // Rock     / Lose / Draw / Win
  B: { X: 1, Y: 2, Z: 3 }, // Paper    / Lose / Draw / Win
  C: { X: 2, Y: 3, Z: 1 } //  Scissors / Lose / Draw / Win
}

let scoreP1 = 0
let scoreP2 = 0

// Puzzle 1
for (let i = 0; i < array.length; i++) {
  const [theirs, mine] = array[i].split(' ')
  if (!theirs || !mine) {
    continue
  }
  // Puzzle 1
  const result = rules[theirs][mine]
  scoreP1 += selPoints[mine]
  scoreP1 += resPoints[result]

  // Puzzle 2
  scoreP2 += resPoints2[mine]
  scoreP2 += rules2[theirs][mine]
}

console.log('Puzzle 1 Score: ', scoreP1)
console.log('Puzzle 2 Score: ', scoreP2)
