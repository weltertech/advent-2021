const fs = require('fs')
const full = fs.readFileSync('09.txt').toString().split('\n')

const handleInput = (lines, knotCount) => {
// load up the 2d array
  const tailPositions = new Set()

  const knots = []
  for (let kc = 0; kc < knotCount; kc++) {
    knots.push({ r: 0, c: 0 })
  }

  const moveKnot = (idx, rowCol, change) => {
    knots[idx] = {
      ...knots[idx],
      [rowCol]: knots[idx][rowCol] + change
    }
  }

  const qMoveKnot = (idx, first, second, rowCol) => {
    if (first[rowCol] - second[rowCol] > 0) {
      moveKnot(idx, rowCol, 1)
    } else {
      moveKnot(idx, rowCol, -1)
    }
  }

  const move = (rowCol, change, times) => {
    for (let m = 0; m < times; m++) {
      // move H
      moveKnot(0, rowCol, change)

      let prev = 0
      for (let k = 1; k < knots.length; k++) {
        // move all others
        switch (true) {
          case (knots[prev].c === knots[k].c && Math.abs(knots[prev].r - knots[k].r) > 1): // apart on same row
          // move towards on row
            qMoveKnot(k, knots[prev], knots[k], 'r')
            break
          case (knots[prev].r === knots[k].r && Math.abs(knots[prev].c - knots[k].c) > 1): // apart on same col
          // move towards on col
            qMoveKnot(k, knots[prev], knots[k], 'c')
            break
          case (Math.abs(knots[prev].c - knots[k].c) > 1 || Math.abs(knots[prev].r - knots[k].r) > 1): // apart on angle
            const currentH = { ...knots[prev] } // eslint-disable-line
            const currentT = { ...knots[k] } // eslint-disable-line
            qMoveKnot(k, currentH, currentT, 'c')
            qMoveKnot(k, currentH, currentT, 'r')
            break
        }
        prev = k
      }
      tailPositions.add(`${knots[knotCount - 1].r}-${knots[knotCount - 1].c}`)
    }
  }

  tailPositions.add('0-0')
  lines.forEach(line => {
    const [direction, strCount] = line.split(' ')
    const count = parseInt(strCount, 10)
    // UP/Down +/- row
    // Right/Left +/- col

    switch (direction) {
      case 'U': move('r', 1, count)
        break
      case 'D': move('r', -1, count)
        break
      case 'R': move('c', 1, count)
        break
      case 'L': move('c', -1, count)
        break
    }
  })

  console.log('tail positions', tailPositions.size)
}

handleInput(full, 10)
