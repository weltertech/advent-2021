
const { inputMonkees, common } = require('./11_input')

const handleRound = (monkees) => {
  for (let m = 0; m < monkees.length; m++) {
    while (monkees[m].items.length > 0) {
      monkees[m].inspections++
      const item = monkees[m].items.shift()
      // const worry = Math.floor(monkees[m].getWorry(item) / 3) // part 1
      const worry = monkees[m].getWorry(item) % common // part 2
      const targetMonkey = monkees[m].getTarget(worry)
      monkees[targetMonkey].items.push(worry)
    }
  }
}

const handleRounds = (roundCount, monkees) => {
  for (let r = 0; r < roundCount; r++) {
    handleRound(monkees)
  }

  monkees.sort((a, b) => b.inspections > a.inspections ? 1 : -1)
  console.log(monkees[0].inspections * monkees[1].inspections)
}

handleRounds(10000, inputMonkees)
