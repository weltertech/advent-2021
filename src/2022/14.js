const fs = require('fs')
// const example = fs.readFileSync('14_example.txt').toString().split('\n')
const full = fs.readFileSync('14.txt').toString().split('\n')

const grid = []
let lastRow = -1
let lastCol = -1
let minCol = Infinity

const PAD_COL = 100000

const drawHorizontal = (low, high) => {
  console.log('drawHorizontal', low, high)
  if (!grid[low.r]) {
    grid[low.r] = []
    if (low.r > lastRow) {
      lastRow = low.r
    }
  }
  if (low.c < minCol) {
    minCol = low.c
  }
  for (let c = low.c; c <= high.c; c++) {
    grid[low.r][c] = '#'
    if (c > lastCol) {
      lastCol = c
    }
  }
}

const drawVertical = (low, high) => {
  console.log('drawVertical', low, high)
  if (low.c > lastCol) {
    lastCol = low.c
  }
  if (low.c < minCol) {
    minCol = low.c
  }

  for (let r = low.r; r <= high.r; r++) {
    if (!grid[r]) {
      grid[r] = []
    }
    if (r > lastRow) {
      lastRow = r
    }
    grid[r][low.c] = '#'
  }
}

const drawSegment = (start, end) => {
  console.log('drawSegment', start, end)
  if (start.r === end.r) {
    if (start.c > end.c) {
      drawHorizontal(end, start)
    } else {
      drawHorizontal(start, end)
    }
  } else {
    if (start.r > end.r) {
      drawVertical(end, start)
    } else {
      drawVertical(start, end)
    }
  }
}

const isOpen = (r, c) => {
  if (r > lastRow) {
    throw Error('Abyss')
  }

  try {
    return grid[r][c] === undefined
  } catch (e) {
    return true
  }
}

const dropSand = (startPos) => {
  if (!isOpen(startPos.r, startPos.c)) {
    throw Error('FULL')
  }

  let cur = startPos
  let resting = false
  while (!resting) {
    if (cur.r + 1 === lastRow) { // onFloor, stop
    //   console.log('sand resting at', cur)
      if (!grid[cur.r]) {
        grid[cur.r] = []
      }
      grid[cur.r][cur.c] = 'o'
      resting = true
    } else if (isOpen(cur.r + 1, cur.c)) { // drop down
      cur = { r: cur.r + 1, c: cur.c }
    } else if (isOpen(cur.r + 1, cur.c - 1)) { // drop left
      cur = { r: cur.r + 1, c: cur.c - 1 }
    } else if (isOpen(cur.r + 1, cur.c + 1)) { // drop right
      cur = { r: cur.r + 1, c: cur.c + 1 }
    } else {
    //   console.log('sand resting at', cur)
      if (!grid[cur.r]) {
        grid[cur.r] = []
      }
      grid[cur.r][cur.c] = 'o'
      resting = true
    }
  }
}

const addFloor = () => {
  grid[lastRow + 2] = []
  for (let f = 0; f <= lastCol + PAD_COL; f++) {
    grid[lastRow + 2][f] = '#'
  }
  lastRow = lastRow + 2
}

const handleCaveSand = (input, isPartTwo = false) => {
  input.forEach(wall => {
    const endPoints = wall.split(' -> ').map(i => i.trim())
    for (let e = 0; e < endPoints.length - 1; e++) {
      const [sc, sr] = endPoints[e].split(',').map(i => i.trim())
      const [ec, er] = endPoints[e + 1].split(',').map(i => i.trim())
      drawSegment({ r: parseInt(sr, 10), c: parseInt(sc, 10) + PAD_COL }, { r: parseInt(er, 10), c: parseInt(ec, 10) + PAD_COL })
    }
  })

  if (isPartTwo) {
    addFloor()
  }

  console.log('last r: ', lastRow)
  console.log('last c: ', lastCol)

  let dropped = 0
  let abyss = false
  while (!abyss) {
    try {
      dropSand({ c: 500 + PAD_COL, r: 0 })
      dropped++
      if (dropped % 1000 === 0) {
        console.log('dropped', dropped)
      }
    } catch (err) {
      console.log(err)
      abyss = true
    }
  }
  console.log('dropped', dropped)
}

// handleCaveSand(example)
handleCaveSand(full, true)
