const fs = require('fs')
const array = fs.readFileSync('04.txt').toString().split('\n')

const sacks = []
const currentElf = 0

const isCompleteOverlap = (line) => {
  console.log('Line: ', line)
  try {
    const [elf1, elf2] = line.split(',')
    console.log('first split', elf1, elf2)
    const [low1, high1] = elf1.split('-')
    const [low2, high2] = elf2.split('-')
    console.log('rest', low1, high1, low2, high2)

    const l1 = parseInt(low1, 10)
    const h1 = parseInt(high1, 10)
    const l2 = parseInt(low2, 10)
    const h2 = parseInt(high2, 10)

    if ((l1 <= l2 && h1 >= h2) || // if 1 contains 2
            (l2 <= l1 && h2 >= h1)) {
      return true
    }
    return false
  } catch (e) {
    return false
  }
}

const isAnyOverlap = (line) => {
  console.log('Line: ', line)
  try {
    const [elf1, elf2] = line.split(',')
    console.log('first split', elf1, elf2)
    const [low1, high1] = elf1.split('-')
    const [low2, high2] = elf2.split('-')
    console.log('rest', low1, high1, low2, high2)

    const l1 = parseInt(low1, 10)
    const h1 = parseInt(high1, 10)
    const l2 = parseInt(low2, 10)
    const h2 = parseInt(high2, 10)

    for (let i = l1; i <= h1; i++) {
      for (let q = l2; q <= h2; q++) {
        if (i == q) {
          return true
        }
      }
    }
  } catch (e) {
    return false
  }
}

let count = 0
for (let i = 0; i < array.length; i++) {
  if (isAnyOverlap(array[i])) {
    count++
  }
}

console.log(count)
