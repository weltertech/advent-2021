const fs = require('fs')

const example = fs.readFileSync('./13_example.txt')
  .toString()
  .split('\n\n') // split on double line break
  .map(x => x.split('\n')) // split on single line break, then evaluate each line into two arrays
  .map(x => [eval(x[0]), eval(x[1])]) // eslint-disable-line

const full = fs.readFileSync('./13.txt')
  .toString()
  .split('\n\n') // split on double line break
  .map(x => x.split('\n')) // split on single line break, then evaluate each line into two arrays
  .map(x => [eval(x[0]), eval(x[1])]) // eslint-disable-line

const isCorrectOrder = (line1, line2) => {
  for (let i = 0; i < line1.length; i++) {
    const left = line1[i]
    const right = line2[i]

    if (right === undefined) {
      return false // if right runs out of items first, incorrect order
    }
    switch (true) {
      case typeof left === 'number' && typeof right === 'number': // both numbers
        if (left === right) {
          continue
        }
        return left < right
      case typeof left === 'object' && typeof right === 'object': // both lines arrays
        const isCorrectObjs = isCorrectOrder(left, right) // eslint-disable-line
        if (isCorrectObjs === null) {
          console.log('continuing')
          continue
        }
        return isCorrectObjs
      case typeof left === 'object' && typeof right === 'number': // line 1 number, line 2 array
        const isCorrectMix1 = isCorrectOrder(left, [right]) // eslint-disable-line
        if (isCorrectMix1 === null) {
          console.log('continuing')
          continue
        }
        return isCorrectMix1
      case typeof left === 'number' && typeof right === 'object': // line 1 number, line 2 array
        const isCorrectMix2 = isCorrectOrder([left], right) // eslint-disable-line
        if (isCorrectMix2 === null) {
          console.log('continuing')
          continue
        }
        return isCorrectMix2
    }
  }

  if (line2.length > line1.length) {
    return true // left runs out first, CORRECT order
  }
  return null
}

const findIdx = (arr, num) => {
  let found = -1
  arr.every((i, idx) => {
    try {
      if (i[0][0] === num && i[0][1] === undefined) { // oh ho ho...gotcha
        found = idx + 1
        return false
      }
    } catch (err) {

    }
    return true
  })
  return found
}

const solvePart2 = (input) => {
  // put everything in 1 array
  const allSignals = []
  allSignals.push([[2]])
  allSignals.push([[6]])

  for (let i = 0; i < input.length; i++) {
    allSignals.push(input[i][0])
    allSignals.push(input[i][1])
  }

  allSignals.sort((a, b) => isCorrectOrder(a, b) ? -1 : 1)
  console.log(allSignals)

  const divIdx = findIdx(allSignals, 2)
  const div2Idx = findIdx(allSignals, 6)

  console.log(divIdx * div2Idx)
}

// solvePart1(example)
// solvePart1(full)
solvePart2(example)
solvePart2(full)
