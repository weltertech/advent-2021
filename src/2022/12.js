const fs = require('fs')
const dijk = require('./12-find-shortest')
const example = fs.readFileSync('12_example.txt').toString().split('\n')
const full = fs.readFileSync('12.txt').toString().split('\n')

const START_CHAR = 'S'
const END_CHAR = 'E'

const getHeight = (char) => {
  // a 97    1     if lower, charCode - 96
  try {
    if (char === START_CHAR) {
      return 1
    } else if (char === END_CHAR) {
      return 26
    } else {
      return char.charCodeAt(0) - 96
    }
  } catch (err) {
  // eol char or something?
    return -1
  }
}

// const start = {}
// const end = {}
// const stepCount = 0

const grid = []
const RIGHT = 'R'
const LEFT = 'L'
const UP = 'U'
const DOWN = 'D'

// const getDirectionPriority = (c) => {
//   switch (true) {
//     case c.r === end.r && c.c < end.c:
//       return [RIGHT, UP, DOWN, LEFT]
//     case c.r === end.r && c.c > end.c:
//       return [LEFT, DOWN, UP, LEFT]
//     case c.c === end.c && c.r < end.r:
//       return [DOWN, RIGHT, LEFT, UP]
//     case c.c === end.e && c.r > end.r:
//       return [UP, LEFT, RIGHT, DOWN]
//     case c.r > end.r && c.c > end.c: // go up and left
//       return [UP, LEFT, DOWN, RIGHT]
//     case c.r > end.r && c.c < end.c: // go up and right
//       return [UP, RIGHT, DOWN, LEFT]
//     case c.r < end.r && c.c > end.c: // go down and left
//       return [DOWN, LEFT, UP, RIGHT]
//     case c.r < end.r && c.c < end.c:
//       return [DOWN, RIGHT, LEFT, UP]
//   }
//   return [UP, LEFT, DOWN, RIGHT]
// }

const lookFns = {
  [UP]: (c) => c.r === 0 ? -1 : { r: c.r - 1, c: c.c, went: UP },
  [DOWN]: (c) => c.r === grid.length - 1 ? -1 : { r: c.r + 1, c: c.c, went: DOWN },
  [LEFT]: (c) => c.c === 0 ? -1 : { r: c.r, c: c.c - 1, went: LEFT },
  [RIGHT]: (c) => c.c === grid[0].length - 1 ? -1 : { r: c.r, c: c.c + 1, went: RIGHT }
}

// const P = {
//   OFFGRID: -1,
//   BACKTRACK: -1,
//   UP: 3,
//   EVEN: 2,
//   DOWN: 1,
//   END: 5
// }

/*
    console.log('looking', priorities[x])
    if (dont === priorities[x]) {
      // this is the direction you came from, dont backtrack like this
      console.log('   --- backwards continue')
      continue
    }
    // call look ahead
    const lookCoords = lookFns[priorities[x]](p)
    if (lookCoords === -1) {
      console.log('   --- off grid continue')
      continue // this position would be off grid
    }
    if (visited.find(v => v.r === lookCoords.r && v.c === lookCoords.c)) {
      console.log('   --- been there, dont repeat spots')
      continue
    }
    const lookHeight = grid[lookCoords.r][lookCoords.c]
    if ((lookHeight > currentHeight && lookHeight - currentHeight === 1) || lookHeight <= currentHeight) {
      // go up this way
      console.log('moving', priorities[x])
      findPath(lookCoords, lookCoords.dont)
    }
*/
// U / D / L / R
// const path = new Stack()

// let foundLength = Infinity
// let solution = null
// let solutionCount = 0
// const abort = null
// let maxLen = 0

// const findPath = (p, isStart) => {
//   if (!isStart) {
//     path.push(p)
//   }

//   const currentHeight = grid[p.r][p.c]
//   if (path.toArray().length > maxLen) {
//     maxLen = path.toArray().length
//     console.log('max length achieved', maxLen, abort)
//   }

//   if (p.r === end.r && p.c === end.c) {
//     console.log('FOUND END')
//     solutionCount++
//     // we're done
//     if (path.toArray().length < foundLength) {
//       foundLength = path.toArray().length
//       solution = path.toArray()
//     }
//     return
//   }
//   if (path.toArray().length > abort) {
//     console.log('abort! after', abort)
//     return
//   }

//   const directions = getDirectionPriority(p)
//   // console.log('at', p)
//   const opts = []
//   for (let x = 0; x < 4; x++) {
//     // console.log('checking', directions[x])
//     // get my options
//     const look = lookFns[directions[x]](p)

//     if (look === -1) {
//       // dont even add it to opts, continue to next direction
//       continue
//     }

//     if (path.toArray().find(v => v.r === look.r && v.c === look.c)) {
//       // console.log('   --- been there, dont repeat spots')
//       continue
//     }
//     if (look.r === start.r && look.c === start.c) {
//       // console.log('   --- dont return to start')
//       continue
//     }

//     const lookHeight = grid[look.r][look.c]
//     switch (true) {
//       case lookHeight - currentHeight === 1:
//         opts.push({ look, priority: P.UP })
//         break
//       case lookHeight === currentHeight:
//         opts.push({ look, priority: P.EVEN })
//         break
//       case lookHeight < currentHeight:
//         opts.push({ look, priority: P.DOWN })
//     }
//   }
//   // sort my options
//   opts.sort((a, b) => a.priority < b.priority ? 1 : -1)
//   // console.log('options', opts)
//   // exectute in order

//   for (let o = 0; o < opts.length; o++) {
//     findPath(opts[o].look)
//     path.pop()
//   }
// }

const getGraph = (input) => {
  let startNode
  let endNode
  // build the grid
  input.forEach((line, rowIdx) => { // eslint-disable-line
    grid[rowIdx] = []
    for (let c = 0; c < line.length; c++) {
      const spot = line[c]
      if (getHeight(spot) > 0) {
        grid[rowIdx][c] = getHeight(spot)
        if (spot === START_CHAR) {
          endNode = `${rowIdx}-${c}`
        }
        if (spot === END_CHAR) {
          startNode = `${rowIdx}-${c}`
        }
      }
    }
  })

  const graph = {}

  // build graph
  for (let r = 0; r < grid.length; r++) {
    for (let c = 0; c < grid[0].length; c++) {
      const nodeid = `${r}-${c}`
      const node = {}
      const currentHeight = grid[r][c]
      const directions = [UP, DOWN, LEFT, RIGHT]

      for (let x = 0; x < 4; x++) {
        // console.log('checking', directions[x])
        // get my options
        const look = lookFns[directions[x]]({ r, c })
        if (look === -1) {
          // dont even add it to opts, continue to next direction
          continue
        }
        const childid = grid[look.r][look.c] === 1 ? `end${look.r}-${look.c}` : `${look.r}-${look.c}`

        const lookHeight = grid[look.r][look.c]
        switch (true) { // these are valid child node scenarios
          case lookHeight - currentHeight === 1:
          case lookHeight === currentHeight:
          case lookHeight < currentHeight:
            node[childid] = 1 // add child
        }
      }
      graph[nodeid] = node
    }
  }
  return { graph, startNode, endNode: `end${endNode}` }
}

const findShortest1 = (input) => {
  const { graph, startNode, endNode } = getGraph(input)
  console.log('graph', graph, startNode, endNode)
  const result = dijk.findShortestPath1(graph, startNode, endNode)
  console.log(result)
}

const findShortest2 = (input) => {
  const { graph, startNode, endNode } = getGraph(input)
  console.log('graph', graph, startNode, endNode)
  // const result = dijk.findShortestPath2(graph, startNode)
  // console.log(result)
}

// findShortest1(full)
findShortest2(full)
