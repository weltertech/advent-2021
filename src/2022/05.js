const fs = require('fs')
const array = fs.readFileSync('05.txt').toString().split('\n')
const Stack = require('stackjs')

// [Q]     [P] [P]
// [G] [V] [S] [Z] [F]
// [W] [V] [F] [Z] [W] [Q]
// [V] [T] [N] [J] [W] [B] [W]
// [Z] [L] [V] [B] [C] [R] [N] [M]
// [C] [W] [R] [H] [H] [P] [T] [M] [B]
// [Q] [Q] [M] [Z] [Z] [N] [G] [G] [J]
// [B] [R] [B] [C] [D] [H] [D] [C] [N]
//  1   2   3   4   5   6   7   8   9

// move 3 from 6 to 2
// move 5 from 6 to 7
// move 6 from 2 to 5

const stacks = [
  undefined,
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack(),
  new Stack()
]

// load the stacks...starting from the BOTTOM (row 7) to the TOP (row 0)
for (let i = 7; i >= 0; i--) {
  for (let s = 1; s < 10; s++) {
    const char = array[i].charAt((s * 4) - 3)
    if (char !== ' ') {
      stacks[s].push(char)
    }
  }
}

const handlingPart1 = (count, start, end) => {
  console.log(count, start, end)

  for (let m = 0; m < count; m++) {
    try {
      const item = stacks[start].pop()
      console.log('moved', item)
      stacks[end].push(item)
    } catch (e) {
      // happens when nothing left on stack
      console.log('stack empty!')
    }
  }
}

const handlingPart2 = (count, start, end) => {
  const toMove = [] // keep a list of moving crates
  for (let m = 0; m < count; m++) {
    try {
      const item = stacks[start].pop()
      console.log('moved', item)
      toMove.push(item)
    } catch (e) {
      // happens when nothing left on stack
      console.log('stack empty!')
    }
  }
  toMove.reverse() // reverse them before pushing to target stack
  for (let r = 0; r < toMove.length; r++) {
    stacks[end].push(toMove[r])
  }
}

for (let i = 10; i < array.length; i++) {
  const [, count, , start, , end] = array[i].split(' ')
  // handlingPart1(count, start, end)
  handlingPart2(count, start, end)
}

let tops = ''
for (let s = 1; s < stacks.length; s++) {
  tops = `${tops}${stacks[s].peek()}`
}

console.log(tops)
