const fs = require('fs')
const array = fs.readFileSync('01.txt').toString().split('\n')

const elves = []
let currentElf = 0
for (let i = 0; i < array.length; i++) {
  if (array[i].length === 0) {
    elves.push(currentElf)
    currentElf = 0
  } else {
    currentElf += parseInt(array[i], 10)
  }
}

elves.sort((a, b) => a < b ? 1 : -1)

console.log('highest calorie count elf: ', elves[0])
console.log('top 3 calorie sum: ', elves[0] + elves[1] + elves[2])
