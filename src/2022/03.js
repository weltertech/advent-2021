const fs = require('fs')
const array = fs.readFileSync('03.txt').toString().split('\n')

const sacks = []
const currentElf = 0

const commonCharacters = function (string1, string2) {
  let duplicateCharacter = ''
  for (let i = 0; i < string1.length; i += 1) {
    if (duplicateCharacter.indexOf(string1[i]) === -1) {
      if (string2.indexOf(string1[i]) !== -1) {
        duplicateCharacter += string1[i]
      }/* from  w ww  .  java 2  s.  c o  m */
    }
  }
  return duplicateCharacter
}
const upper = 'A'
const upperZ = 'Z'
const lower = 'a'
const lowerZ = 'z'

console.log('A', upper.charCodeAt(0))
console.log('Z', upperZ.charCodeAt(0))
console.log('a', lower.charCodeAt(0))
console.log('z', lowerZ.charCodeAt(0))

// A 65    27    if upper, charCode - 38
// Z 90    52
// a 97    1     if lower, charCode - 96
// z 122   26

let total = 0

// Part 1
for (let i = 0; i < array.length; i++) {
  const currentSack = array[i]
  // console.log(currentSack)
  const part1 = currentSack.substring(0, currentSack.length / 2)
  const part2 = currentSack.substring(currentSack.length / 2)
  // console.log(part1, part2)

  const shared = commonCharacters(part1, part2)
  console.log(shared)

  if (shared && shared.length > 0) {
    const sharedCode = shared.charCodeAt(0)
    if (sharedCode > 96) {
      // this is lower case
      total += sharedCode - 96
    } else {
      // this is upper case
      total += sharedCode - 38
    }
  }
}

const bruteForceYuck = (s1, s2, s3) => {
  for (let a = 0; a < s1.length; a++) {
    for (let b = 0; b < s2.length; b++) {
      for (let c = 0; c < s3.length; c++) {
        if (s1[a] === s2[b] && s2[b] === s3[c]) {
          return s1[a]
        }
      }
    }
  }
}

// Part 2
let total2 = 0
let badgeCount = 0
for (let i = 0; i < array.length; i += 3) {
  const shared = bruteForceYuck(array[i], array[i + 1], array[i + 2])

  if (shared && shared.length > 0) {
    const sharedCode = shared.charCodeAt(0)
    if (sharedCode > 96) {
      // this is lower case
      total2 += sharedCode - 96
    } else {
      // this is upper case
      total2 += sharedCode - 38
    }
  }
  badgeCount++
}

console.log('badge count', badgeCount)
console.log('Part 1 Total: ', total)
console.log('Part 2 Total: ', total2)
