const fs = require('fs')
const { Stack } = require('@datastructures-js/stack')
const array = fs.readFileSync('07.txt').toString().split('\n')

const path = new Stack()
const folders = { }

const addToAllChildren = (size, stack) => {
  const stackCopy = new Stack(stack.toArray())

  while (stackCopy.size() > 0) {
    const pathName = stackCopy.toArray().join('/') // eslint-disable-line
    if (!folders[pathName]) {
      folders[pathName] = 0
    }
    folders[pathName] += size
    stackCopy.pop()
  }
}

for (let i = 0; i < array.length; i++) {
  const [one, two, three] = array[i].split(' ')
  console.log('cmds: ', one, two, three)
  switch (true) {
    case two === 'cd' && three !== '..': // push path to path stack (unless first /)
      path.push(three)
      break
    case three === '..': // pop path from path stack
      path.pop()
      break
    case one === 'dir' || two === 'ls': // do nothing
      break
    case one !== 'dir' && one !== '$': // this should be a file size -- add to total in folders map
      addToAllChildren(parseInt(one, 10), path)
      break
  }
}

// convert to array and sort smaller than 100000
const result = []
Object.keys(folders).forEach((key) => {
  if (folders[key] < 100000) {
    result.push({ path: key, size: folders[key] })
  }
})

result.sort((a, b) => a.size < b.size ? 1 : -1)

// sum under 100000
let total = 0
for (let i = 0; i < result.length; i++) {
  total += result[i].size
}

console.log(result)
console.log('Sum under 100000: ', total)

// Being part 2
const result2 = []
Object.keys(folders).forEach((key) => {
  result2.push({ path: key, size: folders[key] })
})

result2.sort((a, b) => a.size < b.size ? 1 : -1)

const largestSize = result2[0].size
const currentFreeSpace = 70000000 - largestSize

console.log('largestSize: ', largestSize)
console.log('currentFreeSpace: ', currentFreeSpace)

result2.reverse()

for (let x = 1; x < result2.length; x++) {
  if (currentFreeSpace + result2[x].size > 30000000) {
    console.log('Filesize to delete: ', result2[x].size)
    break
  }
}
