const fs = require('fs')
const example = fs.readFileSync('10_example.txt').toString().split('\n')
const full = fs.readFileSync('10.txt').toString().split('\n')

const sumSignalStrengths = (lines, atCycles) => {
  let strength = 0
  let cycle = 1
  let X = 1
  const crt = []

  crt[0] = []

  const drawCrt = (crt) => {
    crt.forEach(row => {
      console.log(row.join(''))
    })
  }

  const drawAt = (row, col, symbol) => {
    if (!crt[row]) {
      crt[row] = []
    }
    crt[row][col] = symbol
  }

  const drawPixel = (cycle, register) => {
    const row = Math.floor((cycle - 1) / 40)
    const col = (cycle - 1) % 40

    if (col >= register - 1 && col <= register + 1) {
      drawAt(row, col, '#')
    } else {
      drawAt(row, col, '.')
    }
  }

  for (let line = 0; line < lines.length; line++) {
    const [instruction, value] = lines[line].split(' ')

    drawPixel(cycle, X)

    if (atCycles.includes(cycle)) {
      strength += (cycle * X)
    }

    if (instruction === 'addx') {
      cycle++
      drawPixel(cycle, X)
      if (atCycles.includes(cycle)) {
        strength += (cycle * X)
      }
      X += parseInt(value, 10)
    }
    cycle++
  }

  drawCrt(crt)

  return strength
}

const exampleStrength = sumSignalStrengths(example, [20, 60, 100, 140, 180, 220])
console.log('example', exampleStrength)

const fullStrength = sumSignalStrengths(full, [20, 60, 100, 140, 180, 220])
console.log('full', fullStrength)
