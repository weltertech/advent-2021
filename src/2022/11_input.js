const common = 7 * 17 * 11 * 13 * 19 * 2 * 5 * 3
const inputMonkees = [
  {
    inspections: 0,
    items: [89, 84, 88, 78, 70],
    getWorry: (old) => old * 5, // new  = old * 5
    getTarget: (input) => input % 7 === 0 ? 6 : 7
  },
  {
    inspections: 0,
    items: [76, 62, 61, 54, 69, 60, 85],
    getWorry: (old) => old + 1,
    getTarget: (input) => input % 17 === 0 ? 0 : 6
  },
  {
    inspections: 0,
    items: [83, 89, 53],
    getWorry: (old) => old + 8,
    getTarget: (input) => input % 11 === 0 ? 5 : 3
  },
  {
    inspections: 0,
    items: [95, 94, 85, 57],
    getWorry: (old) => old + 4,
    getTarget: (input) => input % 13 === 0 ? 0 : 1
  },
  {
    inspections: 0,
    items: [82, 98],
    getWorry: (old) => old + 7,
    getTarget: (input) => input % 19 === 0 ? 5 : 2
  },
  {
    inspections: 0,
    items: [69],
    getWorry: (old) => old + 2,
    getTarget: (input) => input % 2 === 0 ? 1 : 3
  },
  {
    inspections: 0,
    items: [82, 70, 58, 87, 59, 99, 92, 65],
    getWorry: (old) => old * 11,
    getTarget: (input) => input % 5 === 0 ? 7 : 4
  },
  {
    inspections: 0,
    items: [91, 53, 96, 98, 68, 82],
    getWorry: (old) => old * old,
    getTarget: (input) => input % 3 === 0 ? 4 : 2
  }
]

const testMonkees = [
  {
    inspections: 0,
    items: [79, 98],
    getWorry: (old) => old * 19,
    getTarget: (input) => input % 23 === 0 ? 2 : 3
  },
  {
    inspections: 0,
    items: [54, 65, 75, 74],
    getWorry: (old) => old + 6,
    getTarget: (input) => input % 19 === 0 ? 2 : 0
  },
  {
    inspections: 0,
    items: [79, 60, 97],
    getWorry: (old) => old * old,
    getTarget: (input) => input % 13 === 0 ? 1 : 3
  },
  {
    inspections: 0,
    items: [74],
    getWorry: (old) => old + 3,
    getTarget: (input) => input % 17 === 0 ? 0 : 1
  }
]

const testCommon = 23 * 19 * 13 * 17

module.exports = {
  inputMonkees,
  common,
  testMonkees,
  testCommon
}
