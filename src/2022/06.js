const fs = require('fs')
const array = fs.readFileSync('06.txt').toString().split('\n')

const getSignalMarker = (line, chars) => {
  // for each starting character in the line
  for (let x = 0; x < line.length; x++) {
    const set = new Set()
    // for each of how many chars we are looking at
    for (let c = 0; c < chars; c++) {
      // add that character to a set
      set.add(line.charAt(c + x))
    }
    // if the set's size is equal to the number of chars, the set is unique
    if (set.size === chars) {
      return x + chars
    }
  }
  return 'not found'
}

const getSignalMarkerRegex = (line, chars) => {
  for (let x = 0; x < line.length; x++) {
    const test = line.substring(x, x + chars)
    if (test.match(/^(?:([A-Za-z])(?!.*\1))*$/)) { // regex magic...returns true if no repeating chars in the test string
      return x + chars
    }
  }
  return 'not found'
}

const input = array[0]

// tests
console.log(getSignalMarkerRegex('nppdvjthqldpwncqszvftbrmjlhg', 4)) // 6
console.log(getSignalMarkerRegex('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 4)) // 10
// puzzles
console.log(getSignalMarkerRegex(input, 4))
console.log(getSignalMarkerRegex(input, 14))
